## Goal

The main goal is to create an **Angular** application, which is able to **authenticate** a user using the **login** endpoint of the following backend provider:
[https://jwt-test-cscc.herokuapp.com](https://jwt-test-cscc.herokuapp.com).

## Basics

The app should have a **Login** page, which routes to the **home** page upon successful authentication.
The authentication schema looks like the following:

```
{
    email: string;
    password: string;
}
```

The correct credentials (_our one and only lonely user in our simulated database_) which gives you back the token are:

```
{
    email: 'you@codingsans.com',
    password: 'theBestPasswordEverCreated',
}
```

_* nevours chuckle *_

The **home** page should **NOT** be accessible otherwise. This brings us to the next task...

## Guarded page

Add a **route guard** to _guard_ the home page, so that it is only accessible once the user has been authenticated.

## Login

The login page should contain a **form** with two fields: **email** and **password**.
The form has to follow the criteria below:

- **Both** fields are required
- The **email** needs to be an email (duh) - make sure that it only accepts valid email formats
- The password needs to be at least 8 characters long, no need for other fanciness
- Occurring errors must be displayed to the user in a form of your choosing, preferably under the respected form fields
- The login button should be disabled if the form has any errors.

## Home

The home page should display the details inside the token returned from the backend in JSON format (no styling needed here).

## Auth Service

Generate an authentication service using the **Angular CLI**.
This service should implement methods which respectively let the user **login** and **logout**.
The token returned from the backend should be decoded and stored on the frontend (in localStorage).
For this you need to research libraries that help you decode JWT easily.

## Styling

A [screenshot](https://imgur.com/a/SH5ACCa) is can be found for inspiration. Try to get as close to the design visible on the image, certain ratios and smaller details such as icons, colors or fonts can be different. The background is also totally up to you, try to surprise us:)

## Extra task - deploy to Netlify

If all other tasks are completed, you can try to deploy your wonderful application for the whole world to see. Use [Netlify](https://www.netlify.com/), a free and easy to use service to reach this goal.

```
gl hf
```
_Don't be afraid to reach out to us trough slack if you have any questions_

